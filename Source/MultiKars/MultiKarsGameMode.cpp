// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "MultiKarsGameMode.h"
#include "MultiKarsPawn.h"
#include "MultiKarsHud.h"

AMultiKarsGameMode::AMultiKarsGameMode()
{
	DefaultPawnClass = AMultiKarsPawn::StaticClass();
	HUDClass = AMultiKarsHud::StaticClass();
}

void AMultiKarsGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	UE_LOG(LogTemp, Warning, TEXT("%s -- AT LINE :: %d"), *FString(__FUNCTION__), __LINE__);
}

void AMultiKarsGameMode::BeginPlay()
{
	Super::BeginPlay();
}
