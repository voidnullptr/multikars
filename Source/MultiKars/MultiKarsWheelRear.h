// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "VehicleWheel.h"
#include "MultiKarsWheelRear.generated.h"

UCLASS()
class UMultiKarsWheelRear : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UMultiKarsWheelRear();
};



