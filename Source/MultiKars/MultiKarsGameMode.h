// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "MultiKarsGameMode.generated.h"

UCLASS(minimalapi)
class AMultiKarsGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMultiKarsGameMode();

	virtual void PostLogin(APlayerController* NewPlayer) override;

protected:

	virtual void BeginPlay() override;

};



