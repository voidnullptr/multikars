// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "../Components/GoKartMovementComponent.h"
#include "../Components/GoKartMovementReplicator.h"
#include "GoKart.generated.h"



UCLASS()
class MULTIKARS_API AGoKart : public APawn
{
	GENERATED_BODY()

public:

	// Sets default values for this pawn's properties
	AGoKart();

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UGoKartMovementComponent* MovementComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UGoKartMovementReplicator* MovementReplicator = nullptr;

private:

	/** Handle pressing forward or backward */
	void MoveForward(float Value);

	/** Handle pressing left or right */
	void MoveRight(float Value);

	const FString GetEnumText(ENetRole Role) const;
};
