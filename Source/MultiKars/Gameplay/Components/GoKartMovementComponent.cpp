// Fill out your copyright notice in the Description page of Project Settings.

#include "GoKartMovementComponent.h"
#include "GameFramework/GameStateBase.h"


// Sets default values for this component's properties
UGoKartMovementComponent::UGoKartMovementComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGoKartMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UGoKartMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (GetOwnerRole() == ROLE_AutonomousProxy || GetOwner()->GetRemoteRole() == ROLE_SimulatedProxy)
	{
		const FGoKartMove NewMove = CreateGoKartMove(DeltaTime);
		SimulateMove(NewMove);
		LastMove = NewMove;
	}
}

const FGoKartMove UGoKartMovementComponent::CreateGoKartMove(float DeltaTime)
{
	// Create a Move and send it to the Server
	FGoKartMove NewMove;
	NewMove.DeltaTime = DeltaTime;
	NewMove.Throttle = FMath::Clamp<float>(Throttle, -1, 1);
	NewMove.SteeringThrow = FMath::Clamp<float>(SteeringThrow, -1, 1);
	NewMove.TimeStamp = GetWorld()->GetGameState()->GetServerWorldTimeSeconds();

	return NewMove;
}

void UGoKartMovementComponent::SimulateMove(const FGoKartMove& Move)
{
	// How much force we are applying to our car
	FVector Force = GetOwner()->GetActorForwardVector() * MaxDrivingForce * Move.Throttle;
	Force += GetAirResistance();
	Force += GetRollingResistance();

	// Acceleration is m/s^2
	const FVector& Acceleration = Force / Mass;

	// Add velocity each tick
	Velocity += Acceleration * Move.DeltaTime;

	// Velocity is m/s, multiply by seconds = meters and by 100 to get cm (UE4 unit)
	const FVector& Translation = Velocity * 100 * Move.DeltaTime;

	// Apply the rotation from the Steering Throw and DeltaTime
	ApplyRotation(Move.DeltaTime, Move.SteeringThrow);

	// Make sure to set zero velocity if we hit something, else we apply translation
	UpdateLocationFromVelocity(Translation);
}

const FVector UGoKartMovementComponent::GetAirResistance() const
{
	// AirResistance = -Speed^2 * DragCoefficient
	return -Velocity.GetSafeNormal() * Velocity.SizeSquared() * DragCoefficient;
}

const FVector UGoKartMovementComponent::GetRollingResistance() const
{
	// Divided by 100 because GetGravityZ() returns UnrealUnits (cm/s) and we are working with m/s
	// Negative because it's returning a negative value, we want the rolling resistance, the force pushing upwards (Normal)
	float AccelerationDueToGravity = -GetWorld()->GetGravityZ() / 100;

	float NormalForce = Mass * AccelerationDueToGravity;
	return -Velocity.GetSafeNormal() * RollingResistanceCoefficient * NormalForce;
}

void UGoKartMovementComponent::UpdateLocationFromVelocity(const FVector& Translation)
{
	FHitResult SweepHitResult;
	GetOwner()->AddActorWorldOffset(Translation, true, &SweepHitResult);

	// Reset velocity if we hit something
	if (SweepHitResult.IsValidBlockingHit())
	{
		Velocity = FVector::ZeroVector;
	}
}

void UGoKartMovementComponent::ApplyRotation(float DeltaTime, float SteeringThrow)
{
	// Will always return a positive DeltaLocation, if we are going back we need a negative value
	// float DeltaLocation = Velocity.Size() * DeltaTime;

	// Magnitude between forward and velocity times DeltaTime
	// In this case the DotProduct returns the cos0 between the two vectors, if the velocity is going backwards, the cos0 will be negative!
	float DeltaLocation = FVector::DotProduct(GetOwner()->GetActorForwardVector(), Velocity) * DeltaTime;
	float RotationAngle = DeltaLocation / MinTurningRadius * SteeringThrow; // dx = d0 * radius;
	FQuat RotationDelta(GetOwner()->GetActorUpVector(), RotationAngle);

	Velocity = RotationDelta.RotateVector(Velocity);
	GetOwner()->AddActorWorldRotation(RotationDelta);
}

