// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GoKartMovementComponent.generated.h"

USTRUCT()
struct FGoKartMove
{
	GENERATED_BODY()

	UPROPERTY()
	float Throttle = 0.0f;

	UPROPERTY()
	float SteeringThrow = 0.0f;

	UPROPERTY()
	float DeltaTime = 0.0f;

	UPROPERTY()
	float TimeStamp = 0.0f;

	bool IsValid() const
	{
		return FMath::Abs(Throttle) <= 1 && FMath::Abs(SteeringThrow) <= 1;
	}

	FGoKartMove() = default;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MULTIKARS_API UGoKartMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGoKartMovementComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/** Creates a move */
	const FGoKartMove CreateGoKartMove(float DeltaTime);

	/** Simulates the move */
	void SimulateMove(const FGoKartMove& Move);

public :

	const FGoKartMove& GetLastMove() const { return LastMove; }

	void SetVelocity(const FVector& InVelocity) { Velocity = InVelocity; }
	const FVector& GetVelocity() const { return Velocity; }

	void SetThrottle(const float InValue) { Throttle = InValue; }
	const float GetThrottle() const { return Throttle; }

	void SetSteeringThrow(const float InValue) { SteeringThrow = InValue; }
	const float GetSteeringThrow() const { return SteeringThrow; }

private:

	/** The Mass of the car (Kg) */
	UPROPERTY(EditAnywhere)
	float Mass = 1000;

	/** The force applied to the car when the Throttle is fully down (Newtons)*/
	UPROPERTY(EditAnywhere)
	float MaxDrivingForce = 10000;

	// Minimum radius of the car turning circle at full lock (meters).
	UPROPERTY(EditAnywhere)
	float MinTurningRadius = 10;

	/**
	*  Higher means more drag.
	*  Calculated using max values DragCoefficient = AirResistance/Speed^2; DragCoefficient = 10000/25^2; 25m/s is how much max speed we want to have.
	*/
	UPROPERTY(EditAnywhere)
	float DragCoefficient = 16;

	// Higher means more rolling resistance.
	UPROPERTY(EditAnywhere)
	float RollingResistanceCoefficient = 0.015;

	UPROPERTY()
	FVector Velocity = FVector::ZeroVector;

	UPROPERTY()
	float Throttle = 0.0f;

	UPROPERTY()
	float SteeringThrow = 0.0f;

	/** Last move */
	FGoKartMove LastMove;

	/** Calculate Air resistance */
	const FVector GetAirResistance() const;

	const FVector GetRollingResistance() const;

	void UpdateLocationFromVelocity(const FVector& Translation);

	void ApplyRotation(float DeltaTime, float SteeringThrow);
	
};
