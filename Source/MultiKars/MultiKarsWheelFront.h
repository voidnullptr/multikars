// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "VehicleWheel.h"
#include "MultiKarsWheelFront.generated.h"

UCLASS()
class UMultiKarsWheelFront : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UMultiKarsWheelFront();
};



