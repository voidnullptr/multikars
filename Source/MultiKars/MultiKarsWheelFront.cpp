// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "MultiKarsWheelFront.h"

UMultiKarsWheelFront::UMultiKarsWheelFront()
{
	ShapeRadius = 35.f;
	ShapeWidth = 10.0f;
	bAffectedByHandbrake = false;
	SteerAngle = 50.f;
}
