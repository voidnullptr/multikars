// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/HUD.h"
#include "MultiKarsHud.generated.h"


UCLASS(config = Game)
class AMultiKarsHud : public AHUD
{
	GENERATED_BODY()

public:
	AMultiKarsHud();

	/** Font used to render the vehicle info */
	UPROPERTY()
	UFont* HUDFont;

	// Begin AHUD interface
	virtual void DrawHUD() override;
	// End AHUD interface
};
